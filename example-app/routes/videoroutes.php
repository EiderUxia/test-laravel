<?php

use App\Http\Controllers\VideoController;
use App\Http\Controllers\editarVideo;

    Route::get('agregar', [VideoController::class, 'create'])
                ->name('agregar');
    Route::post('agregar', [VideoController::class, 'store'])
                ->name('agregar');
    Route::get('mostrar', [VideoController::class, 'index'])
                ->name('mostrarList');
    Route::delete('eliminar/{id}', [VideoController::class, 'destroy'])
               ->name('eliminar');
    Route::get('editar', [VideoController::class, 'edit'])
               ->name('editar');
    Route::get('mostrarVideo/{id}', [VideoController::class, 'show'])
               ->name('mostrarVideo');
    Route::post('actualizar', [VideoController::class, 'update'])
               ->name('actualizar');
