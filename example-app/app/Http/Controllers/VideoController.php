<?php

namespace App\Http\Controllers;

use App\Models\video;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = video::all(['id', 'titulo', 'descripcion', 'thum', 'link']);
        return response()->json($videos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image_64 = $request->thum; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf      
        $replace = substr($image_64, 0, strpos($image_64, ',')+1);       
      // find substring fro replace here eg: data:image/png;base64,      
       $image = str_replace($replace, '', $image_64);       
       $image = str_replace(' ', '+', $image);       
       $imageName = time().'.'.$extension;      
       Storage::disk('public')->put($imageName, base64_decode($image));
       //Storage::putFile('avatars', ($imageName, base64_decode($image)));
       //Storage::put('$imageName', base64_decode($image));

        $videos = video::create([
            'titulo'=> $request->titulo,
            'descripcion'=> $request->descripcion,
            'thum'=> $imageName,
            'link'=> $request->link,
        ]);
        return redirect("/");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $videos = video::find($id);
        return response()->json($videos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $videos = video::find($request->id);
        return Inertia::render('Edit', [
            'id' => $request->id,
            'titulo' => $videos->titulo,
            'descripcion' => $videos->descripcion,
            'thum' => $videos->thum,
            'link' => $videos->link,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $image_64 = $request->thum; //your base64 encoded data
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf      
        $replace = substr($image_64, 0, strpos($image_64, ',')+1);       
      // find substring fro replace here eg: data:image/png;base64,      
       $image = str_replace($replace, '', $image_64);       
       $image = str_replace(' ', '+', $image);       
       $imageName = time().'.'.$extension;      
       Storage::disk('public')->put($imageName, base64_decode($image));

        $videos = video::find($request->id);
        $videos->titulo = $request->titulo;
        $videos->descripcion = $request->descripcion;
        $videos->thum = $imageName;
        $videos->link = $request->link;
        $videos->save();
        return redirect("/");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\video  $video
     * @return \Illuminate\Http\Response
     */

     
    public function destroy($id)
    {
        $videos = video::find($id);
        $videos->delete();
    }

}
