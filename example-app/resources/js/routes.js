const edit = () => import("./Pages/Edit.vue");

export const route = [
    {
        name: 'editar',
        path: "/editar",
        component: edit,
    },
]